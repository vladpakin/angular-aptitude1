(function () {
	'use strict';

	angular.module('app').config(config);

	/* Constant for API url */
	var API_URL = 'http://localhost:3030/api/';

	config.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$qProvider', '$httpProvider'];

	function config($stateProvider, $urlRouterProvider, $locationProvider, $qProvider, $httpProvider) {

		$httpProvider.interceptors.push(httpInterception);

		$qProvider.errorOnUnhandledRejections(false);
		//$urlRouterProvider.otherwise('/error');
		$urlRouterProvider.otherwise(function ($injector, $location) {
			var $state = $injector.get('$state');

			$state.go('error', { message: "Error 404! Page '" + $location.path() + "' not found." });
		});

		$stateProvider
			.state('home', {
				abstract:    true,
				templateUrl: 'partials/home/home.html',
				controller:  'homeController as vm'
			})
			.state('home.todos', {
				url:         '/',
				templateUrl: 'partials/todos/todos.html',
				controller:  'todosController as vm'
			})
			.state('home.create', {
				url:         '/create',
				templateUrl: 'partials/todo/create-todo.html',
				controller:  'createTodoController as vm'
			})
			.state('error', {
				url:         '/error',
				templateUrl: 'partials/error/error.html',
				controller:  'errorController as vm',
				params:      { message: "Page not found!" }
			});

		$locationProvider.html5Mode(true);
	}

	/**
	 * Handler interceptions for $http provider
	 * For each request is check if url has `.` then just pass it. If we make API call then add API_URL to url in config
	 * For each response we return just data after request to API
	 * @returns {{request: request}}
	 */
	function httpInterception() {
		var handlers = {
			'request':  request,
			'response': response
		};

		return handlers;

		function isHtmlRequest(url) {
			return url.indexOf('.') > -1;
		}

		function request(config) {
			if (isHtmlRequest(config.url)) return config;

			config.url = API_URL + config.url;
			return config;
		}

		function response(response) {
			return isHtmlRequest(response.config.url) ? response : response.data;
		}
	}

})();