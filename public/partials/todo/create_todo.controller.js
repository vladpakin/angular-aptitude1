(function () {

	angular
		.module('app')
		.controller('createTodoController', createTodoController);

	createTodoController.$inject = ['todosFactory', 'notificationService'];

	function createTodoController(todosFactory, notificationService) {
		var vm = this;

		vm.isTodoCreated = false;
		vm.error         = null;

		/* Default levels for importance
		 * Why in object? Because ngModel of angular work with simple array or string very bad
		 * */
		vm.levelsOfImportance = [
			{ level: 1 },
			{ level: 2 },
			{ level: 3 },
			{ level: 4 },
			{ level: 5 }
		];

		/* Default setup for todo */
		vm.defaultTodo = {
			deadline:   new Date(),
			importance: { level: 1 },
			name:       'New TODO!'
		};

		vm.createNewTodo = function () {
			todosFactory.createTodo(vm.defaultTodo)
				.then(function () {
					vm.isTodoCreated = true;
					notificationService.sendNotifications({
						status:  'success',
						message: 'Todo is created!'
					});
				})
				.catch(function (error) {
					vm.error = error.message;
					notificationService.sendNotifications({
						status:  'error',
						message: error.message
					});
				});
		};
	}

})();