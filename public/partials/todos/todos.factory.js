(function () {

	angular
		.module('app')
		.factory('todosFactory', todosFactory);

	todosFactory.$inject = ['$q', 'httpService'];

	function todosFactory($q, httpService) {
		var deferred = $q.defer();

		var service = {
			getTodos:   getTodos,
			updateTodo: updateTodo,
			createTodo: createTodo
		};

		return service;

		/* ----- Service methods ----- */

		/* Get todos from server and update todos values */
		function getTodos() {
			return httpService.get('todos');
		}

		/* Update one todo */
		function updateTodo(todo) {
			return httpService.put('todo', todo);
		}

		function createTodo(todo) {
			if (!isAllFieldDone(todo)) {
				deferred.reject({ message: 'All fields are required!' });
				return deferred.promise;
			}

			/* Flat importance from object to simple string */
			todo.importance = todo.importance.level;
			return httpService.post('todo', todo);
		}

		function isAllFieldDone(todo) {
			return todo.name && todo.deadline && todo.importance;
		}
	}

})();