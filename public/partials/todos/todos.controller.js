(function () {

	angular
		.module('app')
		.controller('todosController', todosController);

	todosController.$inject = ['todosFactory', 'notificationService'];

	function todosController(todosFactory, notificationService) {

		var vm = this;

		vm.todos     = [];
		vm.error     = null;
		vm.isLoading = false;

		/* Invoke request to server to get all todos */
		init();

		/* Controller methods */

		function init() {
			vm.isLoading = true;
			todosFactory.getTodos()
				.then(function (todos) {
					vm.todos     = todos;
					vm.isLoading = false;
				})
				.catch(function (error) {
					vm.error     = error;
					vm.isLoading = false;
					notificationService.sendNotifications({
						status:  'error',
						message: error
					});
				});
		}
	}

})();