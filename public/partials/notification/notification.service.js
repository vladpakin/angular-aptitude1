(function () {

	angular
		.module('app')
		.factory('notificationService', notificationService);
	notificationService.$inject = [];

	function notificationService() {

		/* Array for registering observers */
		var observers = [];

		var service = {
			registerObservable: registerObservable,
			sendNotifications:  sendNotifications
		};

		return service;

		/* Register observers */
		function registerObservable(observer) {
			observers.push(observer);
		}

		/**
		 * Pass through all registered observers and call method `update`.
		 * @param notification - object with two params { status : string ['success' || 'error'], message : string'}
		 */
		function sendNotifications(notification) {
			observers.forEach(function (observer) {
				observer.update(notification);
			});
		}
	}
})();