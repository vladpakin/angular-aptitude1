(function () {

	angular
		.module('app')
		.component('notification', {
			templateUrl: 'partials/notification/notification.view.html',
			controller:  ['$timeout', 'notificationService', NotificationComponent]
		});

	/**
	 * Component for notification of every change in todo.
	 * @param $element
	 * @param todosFactory
	 * @constructor
	 */
	function NotificationComponent($timeout, notificationService) {

		var self = this;

		self.isShowNotify = false;
		self.notification = null;

		self.update = update.bind(this);
		self.close  = closeNotification.bind(this);

		self.$onInit = function () {
			registrationOnFactory();
		};

		/* Register instance of component in factory for listening updates */
		function registrationOnFactory() {
			notificationService.registerObservable(self);
		}

		/* Method will call when something is change in factory */
		function update(notifyMessage) {
			self.notification = notifyMessage;
			self.isShowNotify = true;
			closeNotification();
		}

		/* Notification will close automatically after 3 seconds */
		function closeNotification() {
			$timeout(function () {
				self.isShowNotify = false;
				self.notification = null;
			}, 3000);
		}

	}
})();