(function () {

	angular
		.module('app')
		.service('httpService', httpService);

	httpService.$inject = ['$http'];

	/**
	 * Wrapper around angular $http provider.
	 * @param $http
	 * @returns {{get: get, post: post, put: put}}
	 */
	function httpService($http) {

		var service = {
			get:  get,
			post: post,
			put:  put
		};

		return service;

		/* ----- Service methods ----- */

		function get(url, config) {
			return $http.get(url, config || {});
		}

		function post(url, data, config) {
			return $http.post(url, data || {}, config || {});
		}

		function put(url, data, config) {
			return $http.put(url, data || {}, config || {});
		}
	}
})();