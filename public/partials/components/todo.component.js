(function () {

	var TODO_EXPIRING       = 'expiring',
		TODO_COMPLETE       = 'complete',
		TODO_TODAY          = 'today',
		ELEMENT_FIRST_CHILD = ':first';

	angular
		.module('app')
		.component('todoItem', {
			bindings:    {
				todo: '='
			},
			templateUrl: 'partials/components/todo.component.view.html',
			controller:  ['$element', 'todosFactory', 'notificationService', TodoItem]
		});

	function TodoItem($element, todosFactory, notificationService) {

		var self = this;

		self.toggleComplete = toggleComplete.bind(this);

		self.$onInit = function () {
			markTodo(self.todo);
		};

		/**
		 * Set class depends on todo params
		 * @param todo
		 */
		function markTodo(todo) {
			var deadlineDate = new Date(todo.deadline);
			var nowDate      = new Date();

			/* Mark complete todo as green */
			if (todo.isComplete) {
				setClassByParam(TODO_COMPLETE);
				return;
			}

			/* Mark today todo as yellow */
			if (compareDate(deadlineDate, '===')) {
				setClassByParam(TODO_TODAY);
				return;
			}

			/* Mark expiring todo as red */
			if (compareDate(deadlineDate, '>')) {
				setClassByParam(TODO_EXPIRING);
			}
		}

		/* Compare two date not include hours. Just by day.*/
		function compareDate(deadling, sign) {
			switch (sign) {
				case '===' :
					return new Date().setHours(0, 0, 0, 0).toString() === deadling.setHours(0, 0, 0, 0).toString();
				case '>':
					return new Date().setHours(0, 0, 0, 0).toString() > deadling.setHours(0, 0, 0, 0).toString();
				default:
					return false;
			}
		}

		/* Do not add notification, because user will see that todo is changed. */
		function toggleComplete() {
			todosFactory.updateTodo(self.todo)
				.then(function (newTodo) {
					self.todo = newTodo;
					markTodo(self.todo);
				})
				.catch(function (error) {
					notificationService.sendNotifications({
						status:  'error',
						message: error
					});
				});
		}

		/**
		 * Get first child of element and set css class to it
		 * @param className
		 */
		function setClassByParam(className) {
			var element = $element.children(ELEMENT_FIRST_CHILD);

			/* Clear first child from classes */
			element.removeClass([TODO_EXPIRING, TODO_COMPLETE, TODO_EXPIRING].join(' '));

			element.addClass(className);
		}

	}
})();