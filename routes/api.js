var express    = require('express');
var api        = express.Router();
var apiService = require('../server/services/apiService');

api.get('/todos', function (req, res) {
	apiService.getTodos().then(function (todos) {
		res.send(todos);
	}).catch(function (error) {
		res.statusCode = 500;
		res.send({
			message: "Error getting todos!",
			error:   error
		});
	});

});

api.post('/todo', function (req, res) {
	apiService.addTodo(req.body)
		.then(function (todo) {
			res.send(todo);
		})
		.catch(function (error) {
			res.statusCode = 500;
			res.send({
				message: "Error adding todo!",
				error:   error
			});
		});

});

api.put('/todo', function (req, res) {
	apiService.updateTodo(req.body)
		.then(function (todo) {
			res.send(todo);
		})
		.catch(function (error) {
			res.statusCode = 500;
			res.send({
				message: "Error updating todo!",
				error:   error
			});
		});
});

module.exports = api;
