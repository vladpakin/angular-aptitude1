module.exports = [
	{
		id:         1,
		name:       "The first task",
		deadline:   "2017-02-15T14:35:01Z",
		importance: 1,
		isComplete: false
	},
	{
		id:         2,
		name:       "The second task",
		deadline:   "2017-01-01T14:35:01Z",
		importance: 1,
		isComplete: false
	},
	{
		id:         3,
		name:       "The third task",
		deadline:   "2017-02-28T14:35:01Z",
		importance: 2,
		isComplete: false

	},
	{
		id:         4,
		name:       "The fourth task",
		deadline:   "2018-01-01T14:35:01Z",
		importance: 1,
		isComplete: true
	},
	{
		id:         5,
		name:       "The fifth task",
		deadline:   "2017-08-01T14:35:01Z",
		importance: 3,
		isComplete: false
	}
];