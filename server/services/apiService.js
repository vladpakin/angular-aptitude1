var Q = require('q');

var mockdata = require('../data/mockdata');

module.exports = {
	getTodos: function () {
		var deferred = Q.defer();

		deferred.resolve(mockdata);

		return deferred.promise;
	},

	addTodo: function (todo) {
		var deferred = Q.defer();

		/* Generate id and set isComplete for new todo  */
		todo.id         = mockdata.length + 1;
		todo.isComplete = false;

		/* Update mock data */
		mockdata.push(todo);

		/* Return newly created todo */
		deferred.resolve(todo);

		return deferred.promise;
	},

	updateTodo: function (todo) {
		var deferred = Q.defer();

		var index = mockdata.map(function (element) {
			return element.id;
		}).indexOf(todo.id);

		mockdata[index] = todo;

		deferred.resolve(todo);

		return deferred.promise;

	}
};
